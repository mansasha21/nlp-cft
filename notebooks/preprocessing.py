import pandas as pd
import pymorphy2
from gensim.models import KeyedVectors
import re
import numpy as np


remove_punctuation = lambda x: np.asarray(re.subn(r'[^\w\s%]', ' ', x.lower())[0].split())
url_replacement = lambda x: re.sub(r'(?:http[^\s]+)($|\s)', r'url\1', x)
user_replacement = lambda x: re.sub(r'(?:@[^\s]+)($|\s)', r'user\1', x)

MAX_LEN_tkk = 31
MAX_LEN_bank = 30
MODEL_PATH = '../models/ft/model.model'


def preprocess(text_data, max_len):
    """
    params:
        text_data : pd.Series
            text in format samlpes*sentences
        max_len : int
            max_words in sentence
    """
    text_data = text_data.apply(url_replacement)
    text_data = text_data.apply(user_replacement)
    text_data = text_data.apply(remove_punctuation)

    ft = KeyedVectors.load(MODEL_PATH)
    morph = pymorphy2.MorphAnalyzer()
    
    get_embed = lambda x: ft.get_vector(x[0])
    get_prepared_data = lambda x: np.asarray(list(map(get_embed, map(morph.normal_forms, x))))
    
    X = text_data.apply(get_prepared_data).values
    add_padding = lambda x: np.concatenate((x, np.zeros((max_len - x.shape[0], x.shape[1]))), axis=0)
    X = np.asarray(list(map(add_padding, X)))
    X = np.expand_dims(X, axis=-1)
    
    return X
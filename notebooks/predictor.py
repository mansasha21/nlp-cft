from tensorflow.keras.preprocessing.sequence import pad_sequences 
from tensorflow.keras.layers import Softmax, Dense, Conv2D, Dropout, Flatten, MaxPooling2D
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow import keras
from tensorflow.keras import backend as K


def weighted_categorical_crossentropy(weights):
    weights = K.variable(weights)

    def loss(y_true, y_pred):
        y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        loss = y_true * K.log(y_pred) * weights
        loss = -K.sum(loss, -1)

        return loss

    return loss


class Model:
    def __init__(self, inp_shape, num_classes, path_to_best_model, class_weights=None, lr=0.001):

        if class_weights is None:
            self.loss = 'categorical_crossentropy'
        else:
            self.loss = weighted_categorical_crossentropy(class_weights)

        self.model = keras.Sequential([
            Conv2D(15, kernel_size=(3, 300), strides=(1, 1),
                activation='relu', input_shape=inp_shape),
            Dropout(0.5),
            Conv2D(30, kernel_size=(2, 1), strides=(1, 1),
                activation='relu', input_shape=inp_shape),
            #MaxPooling2D(pool_size=(2,1)),
            Flatten(),
            Dense(1000, activation='relu'),
            Dense(num_classes, activation='softmax')
        ])

        self.es = EarlyStopping(min_delta=0.001, patience=10, monitor='val_loss')
        self.mcheck = ModelCheckpoint(path_to_best_model, 
                                monitor='val_loss',
                                save_best_only=True,
                                verbose=1)
        self.model.compile(optimizer=keras.optimizers.Adam(learning_rate=lr),
                      loss=self.loss,
                      metrics='accuracy')
        
    def fit(self, X, y, batch_size=50, epochs=50, validation_data=None):
        return self.model.fit(X, y,batch_size=batch_size, epochs=epochs,
                              validation_data=validation_data,
                              callbacks=[self.es, self.mcheck])

    
class Predictor:
    def __init__(self, path_to_model, loss=None):
        if loss is None:
            self.model = keras.models.load_model(path_to_model)
        else:
            self.model = keras.models.load_model(path_to_model, custom_objects={"loss":loss})
    
    def predict(self, X):
        return self.model.predict(X)